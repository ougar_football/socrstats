test_that("we have test data available", {
  netcheck()
  if (nrow(testdata)<10000) {
    testdata      <<- fetch_data("Denmark")
    testdata_2010 <<- fetch_data("Denmark",2010)
  }

  # Test that test dataset was read correctly
  expect_gt(nrow(testdata),10000)
  expect_equal(nrow(testdata_2010),33*6*2)
})

test_that("global testdata is available", {
  netcheck()
  expect_gt(nrow(testdata),10000)
  expect_equal(nrow(testdata_2010),33*6*2)
})

test_that("season filters work", {
  netcheck()
  # Extract matches from a specific season
  f1 <- filter_season(testdata,2010)
  expect_equal(nrow(f1),2*33*6)
  expect_message(filter_season(testdata,2012,verbose=T), regexp="Only season 2012 kept")

  # Extract matches from season range
  f2 <- filter_seasons(testdata,2011,2013)
  expect_equal(nrow(f2),3*2*33*6)          # Number of matches should be correct
  expect_equal(unique(f2$season),c(2011:2013))    # Only the selected seasons should exist

  # Should do the same with args in reverse
  f3 <- filter_seasons(testdata,2013,2011)
  expect_equal(f2,f3)
  expect_message(filter_seasons(testdata,2012,2013,verbose=T), regexp="Only seasons 2012 to 2013 kept")

  # Filter periods, where both season and round are specified
  f1 <- filter_period(testdata,season_from=2000, round_from=10, season_to=2001, round_to=20)
  expect_equal(nrow(f1),44*6*2)
  f1 <- filter_period(testdata, season_to=2020, round_to=10)
  expect_equal(nrow(f1),10*2*7)
  f1 <- filter_period(testdata, season_from=2020, round_from=10)
  expect_false(any(f1$season<2020))
  expect_gt(sum(f1$season>=2020), 400)
})

test_that("simple filter work", {
  netcheck()
  # Keep only home or away games
  home <- filter_home(testdata_2010)
  away <- filter_away(testdata_2010)
  expect_equal(nrow(home),33*6)
  expect_equal(nrow(away),33*6)
  expect_equal(unique(home$place),"home")
  expect_equal(unique(away$place),"away")

  # Filter a specific club
  agf <- filter_club(testdata_2010, "AGF")
  expect_equal(nrow(agf),33)
  expect_equal(unique(agf$club),"AGF")
  expect_warning(filter_club(testdata_2010,"NoName"))
})

test_that("general filter works", {
  netcheck()
  # Check gfilter. Filter on anything.
  f1 <- gfilter(testdata_2010, "points==3")
  expect_equal(sum(f1$points!=3),0)         # All matches should now have 3 points
  expect_equal(sum(f1$goalf<=f1$goala),0)   # All matches should now have goalf > goala
})
