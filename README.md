
<!-- README.md is generated from README.Rmd. Please edit that file -->

# socRstats <img src='man/figures/logo.png' align="right" height="139" />

<!-- badges: start -->

<!-- badges: end -->

R package which can be used to fetch football data (currently only the
Danish Superliga) and perform data analysis, modelling and
visualisations.

Currently you can fetch data, calculate various tables and find record
streaks. Hopefully more will come shortly.

We aim to give people a possibility to examine football data and perform
data analysis themselfes. The idea is not to use this, to just lookup
“standard” stuff. You can find that online on a thousand different
websites. The idea here is, that you have total flexibility, but you
will need to code it yourself, this package just supplies some data and
some building blocks. So you must be able to program in R or willing to
learn it, to be able to use this package beyond the bare minimum
analysis build into the package.

## Installation

This package is not on CRAN (yet), so you need to install directly from
the Gitlab repo.

You need to be running R. If you do not have your own R server
installed, you can use e.g. <https://rstudio.cloud/>.

The eassiest way to install socRstats is to install the devtools
package, and then run

``` r
devtools::install.gitlab("ougar_football/socRstats")
```

## Getting started

When the package is installed, you can fetch data for the Danish
Superliga, by running

``` r
dk <- socRstats::fetch_data("Denmark")
#> Fetching data from Superstats for Danish Superliga - season all
```

This will return a dataframe, with all matches in the history of the
Danish Superliga up to and including the latest round. No live-data
though. Data is only updated after the games are finished and sometimes,
it can take some time. We are only 2 guys, who also need vacation
sometimes.

## Data structure

The data is in a format, where each game is represented by 2 rows in the
dataset. 1 for each team. This makes it MUCH simpler to do all kinds of
statistics, since we don’t need to constantly check for home/away teams
or home/away goals and count information for 2 teams for each row.

In stead the data looks like this (some columns
excluded):

| id   | season | round | club | opponent | place | goalf | goala | points |
| ---- | ------ | ----- | ---- | -------- | ----- | ----- | ----- | ------ |
| 5885 | 2021   | 1     | SDR  | FCM      | home  | 2     | 0     | 3      |
| 5885 | 2021   | 1     | FCM  | SDR      | away  | 0     | 2     | 0      |
| 5887 | 2021   | 1     | LBK  | AaB      | home  | 0     | 0     | 1      |
| 5887 | 2021   | 1     | AaB  | LBK      | away  | 0     | 0     | 1      |
| …    | …      | …     | …    | …        | …     | …     | …     | …      |

To people juggling football data: DO THIS. It makes your life much
simpler.

It is easy to move between this format and a “normal” format, with 1 row
for each match. Just select all home-games. The plan is to add
functionality to the package to do just that, since we need to be able
to do it when we add data from more countries, since all data we can
fetch from the internet i 1-row-per-match.

## Examples

### Calculate the current standings

``` r
dk <- socRstats::fetch_data("Denmark")
#> Fetching data from Superstats for Danish Superliga - season all
r <- socRstats::table_current(dk)
r
#> # A tibble: 12 x 13
#>      pos club  matches   win  draw  lose pointavg points goalf goala goaldif
#>    <int> <chr>   <int> <int> <int> <int>    <dbl>  <int> <int> <int>   <int>
#>  1     1 FC M…      13     8     3     2    2.08      27    23    14       9
#>  2     2 Brøn…      13     9     0     4    2.08      27    26    19       7
#>  3     3 AGF        13     7     3     3    1.85      24    26    16      10
#>  4     4 Rand…      13     7     1     5    1.69      22    21    12       9
#>  5     5 Sønd…      13     6     3     4    1.62      21    21    17       4
#>  6     6 FC K…      13     6     2     5    1.54      20    22    21       1
#>  7     7 AaB        13     5     4     4    1.46      19    15    18      -3
#>  8     8 FC N…      13     4     4     5    1.23      16    22    19       3
#>  9     9 OB         13     4     4     5    1.23      16    18    20      -2
#> 10    10 Vejl…      13     4     3     6    1.15      15    18    24      -6
#> 11    11 AC H…      13     1     3     9    0.462      6    10    24     -14
#> 12    12 Lyng…      13     0     4     9    0.308      4    12    30     -18
#> # … with 2 more variables: attendance_total <int>, attendance_avg <dbl>
```

### Find Esbjerg’s 5 longest streaks without losing

Fetch the data -\> Extract the matches with Esbjerg -\> Find longest
streak of matches where points greater than 0.

``` r
library(socRstats)
s <- fetch_data("Denmark") %>% filter_club("Esbjerg fB") %>% streak("points>0",limit=5)
#> Fetching data from Superstats for Danish Superliga - season all
s
#> # A tibble: 5 x 11
#>   club   date_from date_to matches   win  draw  lose pointavg points goalf goala
#>   <chr>  <chr>     <chr>     <int> <int> <int> <int>    <dbl>  <int> <int> <int>
#> 1 Esbje… 2003-11-… 2004-0…      10     5     5     0     2        20    30    14
#> 2 Esbje… 2005-04-… 2005-0…       7     5     2     0     2.43     17    19     5
#> 3 Esbje… 2008-11-… 2009-0…       7     3     4     0     1.86     13     8     4
#> 4 Esbje… 2003-05-… 2003-0…       6     3     3     0     2        12    16     7
#> 5 Esbje… 2003-07-… 2003-0…       6     4     2     0     2.33     14    13     6
```
