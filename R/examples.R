# Examples
#
#

#' my_examples
#'
#' @import dplyr
#' @importFrom magrittr %>%
#'
#' @return Nothing
#' @export
#'
my_examples <- function() {

  # Fetch data from the Danish Superliga
  data <- fetch_data("Denmark")

  # Calculate current standings
  tnow <- table_current(data)

  # Find stats for the last 10 seasons for a single team
  agf <- data %>% filter_club("AGF") %>% filter_seasons(2010,2020) %>% club_seasons()

  # Find top-5 of longest streak in danish football with scoring 3+ goals
  goals <- data %>% streak("goalf>2",limit = 5)
}
